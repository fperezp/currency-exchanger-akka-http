# Rate Exchanger

This is a pet project for playing with Akka Http and its ecosystem. It uses `https://exchangeratesapi.io/` under the 
scenes to get currency rates on demand.

The app has been split into 2 different modules, app and model. It has been done this way to allow any other application 
integrate with this Exchanger as easier as possible. Model package provides models for the public API as well as a tiny 
serialization client based on spray-json (it has been chosen because it's part of the Akka-Http ecosystem.)
  
## How to run the app

sbt runApp

## Usage

```
curl -X POST \
  http://localhost:8080/api/convert \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
"fromCurrency": "EUR",
"toCurrency" : "USD",
"amount" : 1050.5 }'
```

_Swagger UI available at:_ http://localhost:8080/swagger

## Run Tests
sbt test

## Code Coverage
sbt clean coverage test coverageReport coverageAggregate

## Next steps
- Tests.
