package exchanger

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.RouteConcatenation
import com.github.swagger.akka.SwaggerSite
import com.typesafe.config.ConfigFactory
import exchanger.Config.{CacheConfig, HttpExchangeConfig, ServerConfig}
import exchanger.http.CachedHttpKafkaStreamsClient
import exchanger.http.route.{Exchange, SwaggerDocs}
import exchanger.service.{HttpExchangeService, InMemorySchemaCacheService}

import scala.concurrent.ExecutionContext

object Exchanger extends App with RouteConcatenation with SwaggerSite {

  sys.addShutdownHook(system.terminate())

  implicit val system: ActorSystem = ActorSystem("exchanger")
  implicit val ex: ExecutionContext = system.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()

  val config = ConfigFactory.load()

  val appConfig = config.getConfig("exchanger")

  val exchangeConfig = HttpExchangeConfig(appConfig)
  val serverConfig = ServerConfig(config.getConfig("server"))
  val cacheConfig = CacheConfig(appConfig)

  val cacheService = new InMemorySchemaCacheService(cacheConfig)
  val httpClient = new CachedHttpKafkaStreamsClient(cacheService)
  val exchangeService = new HttpExchangeService(exchangeConfig, httpClient)

  val exchangeHttpService = new Exchange(exchangeService)
  val swaggerDocsService =  new SwaggerDocs()

  val routes = exchangeHttpService.routes ~ swaggerDocsService .routes ~ swaggerSiteRoute

  Http().bindAndHandle(routes, serverConfig.host, serverConfig.port)
}