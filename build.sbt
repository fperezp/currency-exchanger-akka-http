import sbt.Keys._

val appName = "landoop-exchanger"
val appVersion = "1.0"
val sVersion = "2.12.8"
val akkaV = "2.5.22"
val akkaHttpV = "10.1.8"
val logbackV = "1.2.3"
val scalaLoggingV = "3.9.2"
val mockitoV = "1.10.19"
val scalaTestV = "3.0.5"

resolvers += Resolver.sonatypeRepo("releases")

val appDependencies = Seq(
  "javax.ws.rs"                   % "javax.ws.rs-api"       % "2.0.1",
  "com.typesafe.akka"             %% "akka-http"            % akkaHttpV,
  "com.typesafe.akka"             %% "akka-stream"          % akkaV,
  "ch.qos.logback"                % "logback-classic"       % logbackV,
  "com.typesafe.scala-logging"    %% "scala-logging"        % scalaLoggingV,
  "com.github.swagger-akka-http"  %% "swagger-akka-http"    % "2.0.2",
  "com.github.swagger-akka-http"  %% "swagger-scala-module" % "2.0.3",
  "co.pragmati"                   %% "swagger-ui-akka-http" % "1.2.0",
  "org.mockito"                   % "mockito-all"           % mockitoV        % Test,
  "org.scalatest"                 %% "scalatest"            % scalaTestV      % Test,
  "com.typesafe.akka"             %% "akka-stream-testkit"  % akkaV           % Test,
  "com.typesafe.akka"             %% "akka-http-testkit"    % akkaHttpV       % Test)

val modelDependencies = Seq(
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV)

lazy val model = (project in file("model"))
  .settings(name := appName + "-model")
  .settings(version := appVersion)
  .settings(scalaVersion := sVersion)
  .settings(libraryDependencies ++= modelDependencies)

lazy val app = (project in file("app"))
  .settings(name := appName + "-app")
  .settings(version := appVersion)
  .settings(scalaVersion := sVersion)
  .settings(libraryDependencies ++= appDependencies)
  .dependsOn(model)

lazy val root = (project in file("."))
  .settings(name := appName)
  .settings(version := appVersion)
  .settings(scalaVersion := sVersion)
  .aggregate(app, model)
  .dependsOn(app, model)

addCommandAlias("runApp", ";project app; run")
