package exchanger.service


import exchanger.service.InMemorySchemaCacheService.internal.CachedObject
import java.util.concurrent.ConcurrentHashMap

import akka.http.scaladsl.model.HttpResponse
import com.typesafe.scalalogging.LazyLogging
import exchanger.Config.CacheConfig

import scala.concurrent.{ExecutionContext, Future}
import scala.language.higherKinds

trait CacheService[F[_], A, B] {

  def cache(key: A, collectResource: => F[B])(implicit ec: ExecutionContext): F[B]
}

/**
  * Cache implementation for services returning a Future[HttpResponse]. It has been implemented to cache Future[T] values
  * instead of T values trying to take care of the thundering herds problem where many requests to a particular cache key
  * (e.g. a resource URI) arrive before the first one could be completed. Inspired in Akka HTTP Cache.
  * (https://doc.akka.io/docs/akka-http/current/common/caching.html#basic-design)
  *
  * @param config: Configuration for this cache service inludes:
  *              - expirationTime: Time to discard a cached response.
  */
class InMemorySchemaCacheService(config: CacheConfig) extends CacheService[Future, String, HttpResponse] with LazyLogging {

  import scala.concurrent.duration._

  val cacheExpirationTime: FiniteDuration = config.expirationTime

  private val cacheMap = new ConcurrentHashMap[String, CachedObject]()

  override def cache(key: String, collectResource: => Future[HttpResponse])(implicit ec: ExecutionContext): Future[HttpResponse] =
    Option(cacheMap.get(key)) match {
      case Some(cachedObject) if !cachedObject.deadline.isOverdue() =>
        logger.debug(s"Cache hit: {}", key)
        cachedObject.rPromise
      case _ =>
        logger.debug(s"Cache miss: {}", key)
          cacheMap.put(key, CachedObject(collectResource, cacheExpirationTime.fromNow))
        collectResource
    }
}

object InMemorySchemaCacheService {
  import scala.concurrent.duration._

  private[service] object internal {
    final case class CachedObject(rPromise: Future[HttpResponse], deadline: Deadline)
  }
}
