package exchanger

object Model {

  final case class ExchangeRequest(
    fromCurrency: String,
    toCurrency: String,
    amount: Double)

  final case class ExchangeResponse(
    exchange: Double,
    amount: Double,
    original: Double)
}
