package exchanger.client.sprayjson

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import exchanger.Model.{ExchangeRequest, ExchangeResponse}
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val exchangeRequestFormat = jsonFormat3(ExchangeRequest)
  implicit val exchangeResponseFormat = jsonFormat3(ExchangeResponse)
}

