package exchanger.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import exchanger.service.CacheService

import scala.concurrent.{ExecutionContext, Future}

abstract class HttpClient() {
  def get(url: String): Future[HttpResponse]
}

/**
  * Simple & tiny http client based on Akka Streams. It allows caching.
  *
  * @param cacheService: service that will manage cache and its expiration
  * @param as: Akka Actor System
  * @param ec: ExecutionContext
  */
class CachedHttpKafkaStreamsClient(cacheService: CacheService[Future, String, HttpResponse])(
  implicit as: ActorSystem, ec: ExecutionContext) extends HttpClient {

  def get(url: String): Future[HttpResponse] = cacheService.cache(url, Http().singleRequest(HttpRequest(uri = url)))
}
