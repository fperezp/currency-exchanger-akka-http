package exchanger.service

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import exchanger.Config.HttpExchangeConfig
import exchanger.http.HttpClient
import exchanger.service.HttpExchangeService.model.{BadRequestError, ServerError}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.{Await, Future}

class HttpExchangeServiceSpec
  extends TestKit(ActorSystem("HttpExchangeServiceSpec"))
    with WordSpecLike
    with ScalaFutures
    with Matchers
    with MockitoSugar {

  import system.dispatcher

  implicit val mat: ActorMaterializer = ActorMaterializer()

  val config = HttpExchangeConfig("localhost", "test")

  val httpClient = mock[HttpClient]

  val base = "GBP"
  val target = "USD"
  val sampleRate = 1.22

  val exchangeService = new HttpExchangeService(config, httpClient)

  "HttpExchangeService" should {

    "return right rate when http service responds properly" in {

      val validResponse = HttpResponse( entity = HttpEntity(
        contentType = ContentTypes.`application/json`,
        string = s"""{"base":"$base","rates":{"$target":$sampleRate},"date":"2019-04-09"}"""
      ))

      when(httpClient.get(anyString())).thenReturn(Future(validResponse))

      whenReady(exchangeService.getRate(base, target)) { rate =>
        rate shouldBe sampleRate
      }
    }

    "return BadRequest when http service responds with 400 status code" in {

      when(httpClient.get(anyString())).thenReturn(Future(HttpResponse(status = StatusCodes.BadRequest)))

      import scala.concurrent.duration._

      intercept[BadRequestError.type] {
        Await.result(exchangeService.getRate("GPB", "USD"), 3 seconds)
      }

    }

    "return ServerError when http service responds with a non 200 status code" in {

      when(httpClient.get(anyString())).thenReturn(Future(HttpResponse(status = StatusCodes.InternalServerError)))

      import scala.concurrent.duration._

      intercept[ServerError.type] {
        Await.result(exchangeService.getRate("GPB", "USD"), 3 seconds)
      }

    }
  }
}
