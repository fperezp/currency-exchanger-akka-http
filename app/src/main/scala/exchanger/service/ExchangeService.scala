package exchanger.service

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import exchanger.Config.HttpExchangeConfig
import exchanger.http.HttpClient
import exchanger.client.sprayjson.JsonSupport
import spray.json.JsObject

import scala.concurrent.{ExecutionContext, Future}

abstract class ExchangeService() {
  def exchange(amount: Double, rate: Double): Double =
    BigDecimal(amount * rate).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble

  def getRate(from: String, to: String): Future[Double]
}

class HttpExchangeService(config: HttpExchangeConfig, httpClient: HttpClient)(
  implicit m: Materializer, ec: ExecutionContext) extends ExchangeService with JsonSupport with LazyLogging {

  override def getRate(from: String, to: String): Future[Double] = {

    import HttpExchangeService.model._

    implicit val httpExchangeResponseFormat = jsonFormat2(HttpExchangeResponse)

    val url = s"${config.host}/${config.url}?base=$from&symbols=$to"

    httpClient.get(url) flatMap { response =>
      response.status match {
        case StatusCodes.OK =>
          logger.info("Exchange service request succeed")
          val responseEntity = Unmarshal(response).to[HttpExchangeResponse]
          responseEntity map { res =>
            val conversionRate = res.rates.fields(to)
            conversionRate.convertTo[Double]
          }
        case StatusCodes.BadRequest =>
          logger.error("Exchange request not valid")
          Future.failed(BadRequestError)
        case error =>
          logger.error("Invalid response[{}]: {}", error.value, error.reason())
          Future.failed(ServerError)
      }
    }
  }
}

object HttpExchangeService {

  object model {

    final case class HttpExchangeResponse(base: String, rates: JsObject)

    sealed abstract class  ExchangeError(msg: String) extends RuntimeException(msg)

    case object BadRequestError extends ExchangeError("Malformed request")

    case object ServerError extends ExchangeError("Internal Server Error")
  }

}

