package exchanger.http.route

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import exchanger.Model.{ExchangeRequest, ExchangeResponse}
import io.swagger.v3.oas.models.ExternalDocumentation


class SwaggerDocs() extends SwaggerHttpService {
  override val apiClasses = Set(classOf[Exchange], classOf[ExchangeRequest], classOf[ExchangeResponse])
  override val host = "localhost:8080"
  override val info = Info(version = "1.0")
  override val externalDocs = Some(new ExternalDocumentation().description("Exchange Rates API").url("https://api.exchangeratesapi.io"))
  override val unwantedDefinitions = Seq("Function1RequestContextFutureRouteResult")
}
