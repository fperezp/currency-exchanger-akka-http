package exchanger

import java.util.concurrent.TimeUnit

import com.typesafe.config.{Config => TypesafeConfig}

import scala.concurrent.duration.FiniteDuration

object Config {

  final case class HttpExchangeConfig(host: String, url: String)

  object HttpExchangeConfig {
    def apply(config: TypesafeConfig): HttpExchangeConfig = HttpExchangeConfig(
      config.getString("host"),
      config.getString("url")
    )
  }

  final case class ServerConfig(host: String, port: Int)

  object ServerConfig {
    def apply(config: TypesafeConfig): ServerConfig = ServerConfig(
      config.getString("host"),
      config.getInt("port")
    )
  }

  final case class CacheConfig(expirationTime: FiniteDuration)

  object CacheConfig {
    def apply(config: TypesafeConfig): CacheConfig = {
      val expirationTime = config.getDuration("cacheExpirationTime")
      new CacheConfig(FiniteDuration(expirationTime.getSeconds, TimeUnit.SECONDS))
    }
  }
}
