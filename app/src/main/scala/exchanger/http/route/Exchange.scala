package exchanger.http.route

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import com.typesafe.scalalogging.LazyLogging
import exchanger.Model.{ExchangeRequest, ExchangeResponse}
import exchanger.client.sprayjson.JsonSupport
import exchanger.service.ExchangeService
import exchanger.service.HttpExchangeService.model.BadRequestError
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.{POST, Path}

import scala.util.{Failure, Success}

@Path("api/convert")
class Exchange(exchangeService: ExchangeService) extends JsonSupport with LazyLogging with Directives {

  val routes = exchange

  @POST
  @Operation(summary = "Exchange amount", description = "Exchange an amount from one currency to another.",
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[ExchangeRequest]), mediaType = "application/json"))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Amount converted response",
        content = Array(new Content(schema = new Schema(implementation = classOf[ExchangeResponse]), mediaType = "application/json"))),
      new ApiResponse(responseCode = "400", description = "Bad Request error"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def exchange: Route =
    pathPrefix("api") {
      path("convert") {
        post {
          entity(as[ExchangeRequest]) { request =>

            onComplete(exchangeService.getRate(request.fromCurrency, request.toCurrency)) {
              case Success(rate) =>
                logger.info("Rate has been fetched: {}", rate)
                val finalAmount = exchangeService.exchange(request.amount, rate)
                val response = ExchangeResponse(rate, finalAmount, request.amount)
                complete(response)
              case Failure(exception) =>
                exception match {
                  case BadRequestError => complete(StatusCodes.BadRequest)
                  case anyError =>
                    logger.error("Unexpected error: {}", anyError.getMessage)
                    complete(StatusCodes.InternalServerError)
                }
            }
          }
        }
      }
    }
}
